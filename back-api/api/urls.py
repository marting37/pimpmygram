from django.conf.urls import url
from django.urls import path, include
from .views import (
    TaskListApiView,
    TaskDetailApiView
)

urlpatterns = [
    path('tasks', TaskListApiView.as_view()),
    path('tasks/<int:task_id>/', TaskDetailApiView.as_view()),
]